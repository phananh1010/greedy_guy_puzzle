This is the solution for the puzzle in which you choose to start at any eligible position (zero) in a board represented as a matrix: 

[[0, 0, 0, 0, 0, 7, 7],

 [0, 0, 0, 0, 0, 0, 0],

 [7, 0, 0, 0, 0, 0, 0],

 [0, 0, 0, 0, 0, 0, 0],

 [0, 7, 0, 7, 0, 0, 0],

 [0, 0, 0, 0, 0, 0, 0],

 [0, 0, 0, 0, 7, 0, 0]]

Then you choose a direction, the unit you control will move toward selected position util it see obstacle, then it will stop. For example, let it choose position (1, 1) and move "east", the next state matrix would be like this:

[[0, 0, 0, 0, 0, 7, 7],

 [0, 1, 1, 1, 1, 1, 1],

 [7, 0, 0, 0, 0, 0, 0],

 [0, 0, 0, 0, 0, 0, 0],

 [0, 7, 0, 7, 0, 0, 0],

 [0, 0, 0, 0, 0, 0, 0],

 [0, 0, 0, 0, 7, 0, 0]]

The goal is to choose a good starting point and move the unit in a way such that  there is no zero at the end.

To use the "greedy_guy_puzzle", simple call:
python puzzle.py

Program will detect if there is a solution or not. If there is a solution, it will be printed out in chronicle order. It use depth-first-search (DFS) approach. I found it is a suitable solution since there is no cycle in the search space, and DFS is quite easy to implement.

The matrix is hardcoded, so you need to modify it in the code.