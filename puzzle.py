__author__ = "Anh P. Nguyen"

import numpy
import copy

def mylog(_text, verbose):
    if verbose==True:
        print _text

#simple moving
def check_inboard(_A, _pos):
    if _pos[0] < 0 or _pos[0] >= len(_A):
        return False
    if _pos[1] < 0 or _pos[1] >= len(_A[0]):
        return False
    return True

def check_stop(_A, _pos):
    if check_inboard(_A, _pos) == False:
        return False
    if _A[_pos[0]][_pos[1]] != 0:
        return False
    return True

def direction_to_nextstep(_pos, _direction):
    if _direction == 'n':
        return _pos[0]-1, _pos[1]
    elif _direction == 's':
        return _pos[0]+1, _pos[1]
    elif _direction == 'w':
        return _pos[0], _pos[1]-1
    elif _direction == 'e':
        return _pos[0], _pos[1]+1
    else:
        raise
    

def move_one_step(_pos, direction='n'):
    if direction == 'n':
        return _pos[0]-1, _pos[1]
    elif direction == 's':
        return _pos[0]+1, _pos[1]
    elif direction == 'w':
        return _pos[0], _pos[1]-1
    elif direction == 'e':
        return _pos[0], _pos[1]+1
    else:
        raise

def move(_A, pos=(1, 1), direction='n', verbose=False):

    new_pos = pos
    if check_stop(_A, new_pos) == False:
        mylog('Error: pos: {}, new pos: {}'.format(pos, new_pos), verbose)
        
    while check_stop(_A, new_pos):
        #do something
        pos = new_pos
        _A[pos[0]][pos[1]] = 1
        new_pos = move_one_step(pos, direction)
        mylog('MOVE: end of step, pos: {}, new_pos: {}, continue?: {}'.format(pos, new_pos, check_stop(_A, new_pos)), verbose=verbose)
    
    return pos


#search alg

def check_full_board(_A):
    for row in _A:
        for cell in row:
            if cell == 0:
                return False
    return True


def get_next_direction(_A, _pos=(3, 3)):
    direction_list = []
    if check_inboard(_A, (_pos[0]-1, _pos[1])) and _A[_pos[0]-1, _pos[1]] == 0:
        direction_list.append('n')
    if check_inboard(_A, (_pos[0]+1, _pos[1])) and _A[_pos[0]+1, _pos[1]] == 0:
        direction_list.append('s')
    if check_inboard(_A, (_pos[0], _pos[1]-1)) and _A[_pos[0], _pos[1]-1] == 0:
        direction_list.append('w')
    if check_inboard(_A, (_pos[0], _pos[1]+1)) and _A[_pos[0], _pos[1]+1] == 0:
        direction_list.append('e')
    return direction_list


def trans_state(_A, pos=(3, 3), verbose=False, step=0, _fringe=[]):
    
    
    next_direction_list = get_next_direction(_A, pos)
    mylog('matrix: \n{}\n has next direction: {} \n given current pos: {}\n'.format(_A, next_direction_list, pos), verbose)
    if next_direction_list == []:
        mylog('no avai direction, end search at {}'.format(pos), verbose)
        if check_full_board(_A):
            print "DEBUG: step: {}, matrix: \n{}, fringe size: {}".format(step, _A, len(_fringe))
            return True, _fringe
        return False, _fringe#True if all entry non zero, False = Failed otherwise
    
    while next_direction_list != []:
        direction = next_direction_list[0]
        next_direction_list = next_direction_list[1:]
        
        _A1 = copy.deepcopy(_A)
        
        next_pos = direction_to_nextstep(pos, direction)
        
        mylog('current pos: {}, before move to next pos{}, at step #{}'.format(pos, next_pos, step), verbose)
        next_pos = move(_A1, next_pos, direction, verbose)
        
        mylog('current pos: {}, after move to next pos{}, at step #{}\n------------'.format(pos, next_pos, step), verbose)
        
        old_size = len(_fringe)
        _fringe.append(_A1)
        trial, _new_fringe = trans_state(_A1, next_pos, verbose, step+1, _fringe)
        if trial == True:
            return True, _new_fringe
        _fringe = _fringe[:old_size]
    return False, _fringe
    

def search(_A):
    
    for i,row in enumerate(_A):
        for j,cell in enumerate(row):
            if _A[i, j] == 0:
                _A[i, j] = 1
                fringe = [_A]
                temp = trans_state(_A, pos=(i, j), verbose=False, step=0, _fringe=fringe)
                if temp[0] == True:
                    print 'Has result at {}, {}'.format(i,j)
                    return True, temp[1]
                _A[i, j] = 0
    print 'END'
    return False, []

A = numpy.array(
[[0, 0, 7, 0, 0],
 [0, 0, 7, 0, 0],
 [0, 0, 7, 0, 0],
 [7, 0, 7, 0, 0],
 [7, 0, 0, 0, 7]])

A2 = numpy.array(
[[0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 7],
 [0, 0, 0, 0, 0, 7],
 [0, 0, 0, 0, 0, 7]])
A2B = numpy.array(
[[0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 7, 0],
 [0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 7],
 [0, 0, 0, 0, 0, 7],
 [0, 0, 0, 0, 0, 7]])
A3 = numpy.array(
[[0, 0, 0, 0, 0, 7, 7],
 [0, 0, 0, 0, 0, 0, 0],
 [7, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 7, 0, 7, 0, 0, 0],
 [0, 0, 0, 0, 0, 0, 0],
 [0, 0, 0, 0, 7, 0, 0]])

if __name__ == "__main__":
    target = A
    print 'find solution for matrix: \n {}'.format(target)
    
    trace = search(A2B)
    if trace[0] == False:
        print 'no solution'
    else:
        print 'solution: '
        for item in trace[1]:
            print item